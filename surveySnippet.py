# DB connection 
class DB:
	__init__(self, db, port, user, password)
		# throw Error if db connection failed, might be handeled in DB class
		db = magicPythonDBLibrary(db, port, user, password)
		if (!db):
			raise Exception('could not connect to database')
		self.db = db

# authentication middleware
class Middleware:
	__init__(self, db):
		self.db = db

	def authUser(self, req):
		# take token look up in token db
		authQuery = 'SELECT * FROM access_tokens at JOIN user u ON u.id = at.user_id WHERE at.token = %s'
		token = self.db.query(authQuery, req.token)
		# no token -> 400
		if (!token):
			raise Exception("token invalid")
		# token expired -> token expired
		if (token.expires < time.time()):
			raise Exception("token expired, please renew")
		# valid token, attach user to req and pass to next middleware
		req.locals.user = token;
		return req


#route handler
class Routes:
	__init__(self, db):
		self.db = db

	def getSurvey(self, req, res):
		# param validation 
		if (!survey_id or !isInt(survey_id) ):
			raise Exception('invalid param survey_id')
		# check for data access permission
		validAction = self.db.query('select id from survey_user where user_id = %s', req.locals.user_id)
		if (!validAction):
			raise Exception('user does not have permission to access this content')
		# call serviceFunction
		survey = Survey.getSurvey(req.params.survey_id)
		# return response with data
		return res.response(survey)

#service function
class Survey:
	def getSurvey(self, survey_id) 
		surveyQuery = 'SELECT * FROM survey WHERE id = :%s'
		survey = db:query(query, survey_id)
		return survey

def main(): 
	# spin up python server
	app = new magicPythonServerLibrary()
	# make new db connection
	db = new DB(PROD_DB, DB_PORT, env_user, env_password)
	# initialize middleware
	mw = new Middleware(db)
	# initialize routes
	routes = new Routes(db)

	app.middleware(mw.authUser())
	app.addRoute('GET', "/survey/:survey_id", routes.getSurvey()): # not really sure how your codebase/python setups routes


if (__name__ == "main"):
	main()

